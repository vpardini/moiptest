package com.pardini.moip.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoipMain {

	private static final String FILENAME = "C:\\moip\\log.txt";

	public static void main(String[] args) {

		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
			Pattern urlPattern = Pattern.compile("(?=request_to=\").*(?=\" )");
			Pattern codePattern = Pattern.compile("(?=response_status=\").*(?=\")");

			Map<String, Integer> mapUrls = new HashMap<String, Integer>();
			Map<String, Integer> mapCodes = new HashMap<String, Integer>();

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				Matcher urlMatcher = urlPattern.matcher(sCurrentLine);
				Matcher codeMatcher = codePattern.matcher(sCurrentLine);

				String urlLine = (urlMatcher.find()?urlMatcher.group().replace("request_to=\"", ""):null);
				String codeLine = (codeMatcher.find()?codeMatcher.group().replace("response_status=\"", ""):null);

				if(urlLine != null){
					Integer count = mapUrls.get(urlLine);
					mapUrls.put(urlLine, (count == null) ? 1 : count + 1);
				}

				if(codeLine != null){
					Integer count = mapCodes.get(codeLine);
					mapCodes.put(codeLine, (count == null) ? 1 : count + 1);
				}
			}

			System.out.println("\nTOP 3 URLs:\n");
			Map<String, Integer> urlsOrd = MapUtil.sortByValue(mapUrls);
			printMap(urlsOrd,3);

			System.out.println("\nContagem de Webhooks:\n");
			Map<String, Integer> codesOrd = new TreeMap<String, Integer>(mapCodes);
			printMap(codesOrd);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void printMap(Map<String, Integer> map){
		printMap(map, null);
	}

	public static void printMap(Map<String, Integer> map, Integer lim){
		int count = 0;
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println("Chave : " + entry.getKey() + " Occurs : "
					+ entry.getValue());
			count++;
			if(lim!= null && count>=lim){
				break;
			}
		}

	}
	
}
